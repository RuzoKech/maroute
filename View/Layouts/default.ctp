<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */

$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->script('masonry.pkgd.min.js');
		echo $this->Html->script('jquery-1.11.1.min.js');
		echo $this->Html->script('script');
		echo $this->Html->script('classie');
		echo $this->Html->script('modernizr.custom');			
		echo $this->Html->script('AnimOnScroll');
		echo $this->Html->script('imagesloaded');
							
		echo $this->Html->css('cake.generic');
		echo $this->Html->css('component');
		echo $this->Html->css('cake.maroute');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('style');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
	
	<div id="header">
		<div id="headercontent">
			<?php echo $this->Html->image('maroute_logo.jpg',array('id'=>'maroutelogo','url'=>'/'));?>
			<?php echo $this->Html->image('contactLogo.jpg',array('id'=>'contactlogo'));?>
			<?php echo $this->Html->image('googleLogo.jpg',array('id'=>'googlelogo'));?>
			<?php echo $this->Html->image('facebookLogo.jpg',array('id'=>'facebooklogo'));?>
		</div>
	</div>

			
	
				<?php echo $this->Session->flash(); ?>
	
				<?php echo $this->fetch('content'); ?>
					
		<div id="footer">
			<?php echo $this->Html->image('mrfooter.jpg',array('id'=>'maroutefooter'));?>
		</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
