<div class="voyages form">
<?php echo $this->Form->create('Voyage'); ?>
	<fieldset>
		<legend><?php echo __('Add Voyage'); ?></legend>
	<?php
		echo $this->Form->input('type_id');
		echo $this->Form->input('societe_id');
		echo $this->Form->input('ville_depart',array('options'=>$villes));
		echo $this->Form->input('ville_arrive',array('options'=>$villes));
		echo $this->Form->input('depart_time');
		echo $this->Form->input('arrive_time');
		echo $this->Form->input('nextday',array('value'=>"false"));
		echo $this->Form->input('tarif');
		echo $this->Form->input('classe');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Menu'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Voyages'), array('action' => 'index')); ?></li>
		<!--<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Type'), array('controller' => 'types', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Societes'), array('controller' => 'societes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Societe'), array('controller' => 'societes', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('controller' => 'villes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ville  Depart'), array('controller' => 'villes', 'action' => 'add')); ?> </li>-->
	</ul>
</div>
