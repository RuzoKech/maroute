<div class="voyages index">
	<h2><?php echo __('Voyages'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('type_id'); ?></th>
			<th><?php echo $this->Paginator->sort('societe_id'); ?></th>
			<th><?php echo $this->Paginator->sort('ville_depart'); ?></th>
			<th><?php echo $this->Paginator->sort('ville_arrive'); ?></th>
			<th><?php echo $this->Paginator->sort('depart_time'); ?></th>
			<th><?php echo $this->Paginator->sort('arrive_time'); ?></th>
			<th><?php echo $this->Paginator->sort('nextday'); ?></th>
			<th><?php echo $this->Paginator->sort('tarif'); ?></th>
			<th><?php echo $this->Paginator->sort('classe'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($voyages as $voyage): ?>
	<tr>
		<td><?php echo h($voyage['Voyage']['id']); ?>&nbsp;</td>
		<td>
			<?php echo $this->Html->link($voyage['Type']['ntype'], array('controller' => 'types', 'action' => 'view', $voyage['Type']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($voyage['Societe']['nsociete'], array('controller' => 'societes', 'action' => 'view', $voyage['Societe']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($voyage['Ville_Depart']['nville'], array('controller' => 'villes', 'action' => 'view', $voyage['Ville_Depart']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($voyage['Ville_Arrive']['nville'], array('controller' => 'villes', 'action' => 'view', $voyage['Ville_Arrive']['id'])); ?>
		</td>
		<td><?php echo h($voyage['Voyage']['depart_time']); ?>&nbsp;</td>
		<td><?php echo h($voyage['Voyage']['arrive_time']); ?>&nbsp;</td>
		<td><?php echo h($voyage['Voyage']['nextday']); ?>&nbsp;</td>
		<td><?php echo h($voyage['Voyage']['tarif']); ?>&nbsp;</td>
		<td><?php echo h($voyage['Voyage']['classe']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $voyage['Voyage']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $voyage['Voyage']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $voyage['Voyage']['id']), null, __('Are you sure you want to delete # %s?', $voyage['Voyage']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Menu'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Voyage'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Types'), array('controller' => 'types', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('List Societes'), array('controller' => 'societes', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('List Villes'), array('controller' => 'villes', 'action' => 'index')); ?> </li>
	</ul>
</div>
