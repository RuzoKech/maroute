<div class="societes form">
<?php echo $this->Form->create('Societe'); ?>
	<fieldset>
		<legend><?php echo __('Add Societe'); ?></legend>
	<?php
		echo $this->Form->input('nsociete');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Menu'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Societes'), array('action' => 'index')); ?></li>
	</ul>
</div>
