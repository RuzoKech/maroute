<div class="societes view">
<h2><?php echo __('Societe'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($societe['Societe']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Nsociete'); ?></dt>
		<dd>
			<?php echo h($societe['Societe']['nsociete']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Menu'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Societe'), array('action' => 'edit', $societe['Societe']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Societe'), array('action' => 'delete', $societe['Societe']['id']), null, __('Are you sure you want to delete # %s?', $societe['Societe']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Societes'), array('action' => 'index')); ?> </li>
		<!--<li><?php echo $this->Html->link(__('New Societe'), array('action' => 'add')); ?> </li>-->
	</ul>
</div>
