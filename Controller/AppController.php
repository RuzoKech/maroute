<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array('Session','Auth' => array(
        		'authenticate' => array(
            	'Form' => array(
                'fields' => array('username' => 'email')
            )
        )
    ));
	public $helpers = array('Form', 'Html', 'Session');
	
	public function beforeFilter(){
		//$this->Auth->allow('home');
		$this->Auth->loginAction = array('controller' => 'users', 'action' => 'login');
	    $this->Auth->loginRedirect = array('controller' => 'pages', 'action' => 'display', 'home');
	    $this->Auth->allow('display');
		$this->Auth->allow('search');
	    //$this->Auth->authorize = 'controller';
	}
	
	public function beforeRender(){
		$this->loadModel('Ville');
		$options = array('order'=>array('Ville.nville'));
		$this->set('villesMaroc' , $this->Ville->find('list',$options));
		
		$this->loadModel('Societe');
		$this->set('societesMaroc' , $this->Societe->find('list'));
	}
	
	
	
}