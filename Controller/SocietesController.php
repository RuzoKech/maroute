<?php
App::uses('AppController', 'Controller');
/**
 * Societes Controller
 *
 * @property Societe $Societe
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class SocietesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Societe->recursive = 0;
		$this->set('societes', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Societe->exists($id)) {
			throw new NotFoundException(__('Invalid societe'));
		}
		$options = array('conditions' => array('Societe.' . $this->Societe->primaryKey => $id));
		$this->set('societe', $this->Societe->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Societe->create();
			if ($this->Societe->save($this->request->data)) {
				$this->Session->setFlash(__('The societe has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The societe could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Societe->exists($id)) {
			throw new NotFoundException(__('Invalid societe'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Societe->save($this->request->data)) {
				$this->Session->setFlash(__('The societe has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The societe could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Societe.' . $this->Societe->primaryKey => $id));
			$this->request->data = $this->Societe->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Societe->id = $id;
		if (!$this->Societe->exists()) {
			throw new NotFoundException(__('Invalid societe'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Societe->delete()) {
			$this->Session->setFlash(__('The societe has been deleted.'));
		} else {
			$this->Session->setFlash(__('The societe could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}
}
