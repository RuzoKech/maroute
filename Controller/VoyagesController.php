<?php
App::uses('AppController', 'Controller');
/**
 * Voyages Controller
 *
 * @property Voyage $Voyage
 * @property PaginatorComponent $Paginator
 * @property SessionComponent $Session
 */
class VoyagesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator', 'Session');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Voyage->recursive = 0;
		$this->set('voyages', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Voyage->exists($id)) {
			throw new NotFoundException(__('Invalid voyage'));
		}
		$options = array('conditions' => array('Voyage.' . $this->Voyage->primaryKey => $id));
		$this->set('voyage', $this->Voyage->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Voyage->create();
			if ($this->Voyage->save($this->request->data)) {
				$this->Session->setFlash(__('The voyage has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The voyage could not be saved. Please, try again.'));
			}
		}
		$this->set('types',$this->Voyage->Type->find('list'));
		$this->set('societes',$this->Voyage->Societe->find('list'));
		$this->set('villes',$this->Voyage->Ville_Depart->find('list'));
		$this->set('villes',$this->Voyage->Ville_Arrive->find('list'));

		//$types = $this->Voyage->Type->find('list');
		//$societes = $this->Voyage->Societe->find('list');
		//$villeDeparts = $this->Voyage->VilleDepart->find('list');
		//$villeArrives = $this->Voyage->VilleArrive->find('list');
		//$this->set(compact('types', 'societes', 'villeDeparts', 'villeArrives'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Voyage->exists($id)) {
			throw new NotFoundException(__('Invalid voyage'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Voyage->save($this->request->data)) {
				$this->Session->setFlash(__('The voyage has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The voyage could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Voyage.' . $this->Voyage->primaryKey => $id));
			$this->request->data = $this->Voyage->find('first', $options);
		}
		$this->set('types',$this->Voyage->Type->find('list'));
		$this->set('societes',$this->Voyage->Societe->find('list'));
		$this->set('villes',$this->Voyage->Ville_Depart->find('list'));
		$this->set('villes',$this->Voyage->Ville_Arrive->find('list'));
		
		//$types = $this->Type->find('list');
		//$societes = $this->Voyage->Societe->find('list');
		//$villeDeparts = $this->Voyage->VilleDepart->find('list');
		//$villeArrives = $this->Voyage->VilleArrive->find('list');
		//$this->set(compact('types', 'societes', 'villeDeparts', 'villeArrives'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Voyage->id = $id;
		if (!$this->Voyage->exists()) {
			throw new NotFoundException(__('Invalid voyage'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Voyage->delete()) {
			$this->Session->setFlash(__('The voyage has been deleted.'));
		} else {
			$this->Session->setFlash(__('The voyage could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}

/**
 * search method
 *
 */
 	public function timeformat($time){
 		if($time['meridian']=='am'){
 			return  $time['hour'].':'.$time['min'].':'.'00';
 		}
		else{
			$hour = $time['hour'];
			$min = $time['min'];
			
			if($hour==12)
				return  '00:'.$time['min'].':'.'00'; 
			
			return  ($time['hour']+12).':'.$time['min'].':'.'00';
		}
 	}
 
 	public function searchoption(){
 		if(isset($this->data['voyages']['depart_time']) &&(isset($this->data['voyages']['societe']) && $this->data['voyages']['societe']!=0)){

 			$options = array('conditions'=>array('Voyage.ville_depart ='=>$this->data['voyages']['depart'],
			'Voyage.ville_arrive ='=>$this->data['voyages']['destination'],
			'Voyage.depart_time >='=>$this->timeformat($this->data['voyages']['depart_time']),
			'Voyage.societe_id ='=>$this->data['voyages']['societe']
			),
			'order' => array('Voyage.depart_time'));
 		}
		elseif(isset($this->data['voyages']['depart_time']) && (!isset($this->data['voyages']['societe']) || $this->data['voyages']['societe']==0) ){

			$options = array('conditions'=>array('Voyage.ville_depart ='=>$this->data['voyages']['depart'],
			'Voyage.ville_arrive ='=>$this->data['voyages']['destination'],
			'Voyage.depart_time >'=>$this->timeformat($this->data['voyages']['depart_time'])
			),
			'order' => array('Voyage.depart_time'));
		}
		elseif(!isset($this->data['voyages']['depart_time']) &&isset($this->data['voyages']['societe'])){
			$options = array('conditions'=>array('Voyage.ville_depart ='=>$this->data['voyages']['depart'],
			'Voyage.ville_arrive ='=>$this->data['voyages']['destination'],
			'Voyage.societe_id ='=>$this->data['voyages']['societe']
			),
			'order' => array('Voyage.depart_time'));
		}
		else{
			$options = array('conditions'=>array('Voyage.ville_depart ='=>$this->data['voyages']['depart'],
			'Voyage.ville_arrive ='=>$this->data['voyages']['destination']),
			'order' => array('Voyage.depart_time'));
		}
		
		return $options;
 	}

	public function search(){
		if ($this->request->is('post')) {
			$this->Voyage->recursive = 0;
			$this->loadModel('Ville');
			
			$depart_time = (isset($this->data['voyages']['depart_time']))?$this->data['voyages']['depart_time'] : 'null';
			$societe_id = (isset($this->data['voyages']['societe']))?$this->data['voyages']['societe']:'null';
			
			$option1 = array('conditions'=>array('Ville.id ='=>$this->data['voyages']['depart']));
			$option2 = array('conditions'=>array('Ville.id ='=>$this->data['voyages']['destination']));
			$options = $this->searchoption();
			
			
			$this->set('ville_dep', $this->Ville->find('first',$option1) );
			$this->set('ville_des', $this->Ville->find('first',$option2) );
			$this->set('voyageSearch',$this->Voyage->find('all',$options));
			//$this->set('voyageSearch',$this->Paginator->paginate());
		}
		else{
			$this->redirect('/');
		}
	}
	
	public function search2(){
	 if($this->request->is('post')){
	 	debug($this->data);
	 }
	}

}








